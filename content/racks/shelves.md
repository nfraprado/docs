---
title: Shelf unit devices
---

# Baobab Dispatcer via Tumbleweed Controller

These shelf Devices may well move to a rack of their own at some point.

# First shelf (Top) 

rk3288-veyron-jaq-cbg-0 connected to servo-v2 via Yoshi Flex and usb. Servo connected to network switch and usb hub. Power to standard power bar.

exynos5800-peach-pi-cbg-0 connected to servov2 via ribbon and usb. Servo connected to network switch and usb hub. Power to standard power bar.

# Second shelf

sun50i-h6-pine-h64-cbg-0 connected to relay power controller and network switch and usb hub. Power to E204460 relay with plug to standard power bar from relay.

sun50i-h6-pine-h64-cbg-1 connected to relay power controller and network switch and usb hub. Power to E204460 relay.

exynos5422-odroidxu3-cbg-0 connected to relay power controller and network switch and usb hub. Power to E204460 relay.

r8a7796-m3ulcb-cbg-0 connected to RasPi reset controller and usb hub and power and network switch. Power to 5v power supply.

r8a7796-m3ulcb-cbg-1 connected to RasPi reset controller and usb hub and power and network switch. Power to 5v power supply.

Raspberry Pi 2 acting as reset controller connected to network switch and usb hub. Power to 5v power supply. 

E204460 relay power controller. Power to standard power bar.
5v power supply to standard power bar.

# Third shelf

Tumbleweed controller connected to usb hub and network switch. Power to PMS-0 socket 2.

Netgear (GSS116E) Network Switch 16 port with 2 empty sockets. Power to standard power bar.
24 port USB hub connected to tumbleweed with 16 connected devices. Power to standard power bar.

# Forth shelf
bcm2836-rpi-2-b-cbg-0 connected to usb hub and network switch. Power to PMS-0 port 1.

minnowboard-max-E3825-cbg-0 connected to network switch. Power to pms-1 port 4.

minnowboard-turbot-E3826-cbg-0 connected to usb hub and network switch. Has 1 hdmi module connected. Power to PMS-1 port 2.

minnowboard-turbot-E3826-cbg-1 connected to usb hub and network switch. Has 1 hdmi module connected. Power to PMS-1 port 3.

rk3288-rock2-square-cbg-0 connected to usb hub and network switch. Power to PMS-0 port 3.

meson-g12b-a311d-khadas-vim3-cbg-0 connected to usb hub and network switch. Power to PMS-1 port 1.

meson-g12b-a311d-khadas-vim3-cbg-1 connected to usb hub and network switch. Power to PMS-0 port 4.

Energenie (ENER019) PMS-0 and PMS-1 connected to Standard power bar. Both have network to the network switch.

# Fifth shelf

Netgear (GSS116E) 16 port network switch with 4 empty sockets. power to standard power bar.

# Sixth shelf (bottom)
tegra124-nyan-big-cbg-0 connected to servov2 via Yoshi flex ribbon and usb. Servo connected to usb hub and network switch. Power to standard power bar.

rk3288-veyron-jaq-cbg-1 connected to servov2 via Yoshi flex ribbon and usb. Servo connected to usb hub and network switch. Power to standard power bar.

# Network Switches

Third Shelf
{{< mermaid >}}
graph LR;
 S["switch"] -->|Port 1 empty| N["none"]
 S -->|Port 2| VIM3["meson-g12b-a311d-khadas-vim3-cbg-0"]
 S -->|Port 3| X["exynos5422-odroidxu3-cbg-0"]
 S -->|Port 4| R8A1["r8a7796-m3ulcb-cbg-1"]
 S -->|Port 5| R8A0["r8a7796-m3ulcb-cbg-0"]
 S -->|port 6| RasRST["Raspberry Pi RST CTRL"]
 S -->|port 7| TWD["Tumbleweed Controller"]
 S -->|port 8 -> onboard| H641["sun50i-h6-pine-h64-cbg-1"]
 S -->|port 9 -> USB adapter| H641
 S -->|port 10 -> onboard| H640["sun50i-h6-pine-h64-cbg-0"]
 S -->|port 11 -> USB adapter| H640
 S -->|port 12 -> servov2| PEACH["exynos5800-peach-pi-cbg-0"]
 S -->|port 13 -> servov2| JAQ0["rk3288-veyron-jaq-cbg-0"]
 S -->|port 14| PWRrelay["Power relay for H64 and XU3"]
 S -->|Port 15| S2["Switch on Fifth shelf"]
 S -->|Port 16| C["Core Switch"]
{{< /mermaid >}}

Fifth shelf
{{< mermaid >}}
graph LR;
 S["Switch"] -->|port 1 empty| N["none"]
 S -->|port 2 empty| N2["none"]
 S -->|port 3 empty| N3["none"]
 S -->|port 4 -> servov2| NYAN["tegra124-nyan-big-cbg-0"]
 S -->|port 5 -> servov2| JAQ1["rk3288-veyron-jaq-cbg-1"]
 S -->|port 6 empty| N4["none"]
 S -->|port 7| RKSQ["rk3288-rock2-square-cbg-0"]
 S -->|port 8| TURBOT0["minnowboard-turbot-E3826-cbg-0"]
 S -->|port 9| RASPI["bcm2836-rpi-2-b-cbg-0"]
 S -->|port 10| VIM3["meson-g12b-a311d-khadas-vim3-cbg-1"]
 S -->|port 11| MAX["minnowboard-max-E3825-cbg-0"]
 S -->|port 12| TURBOT1["minnowboard-turbot-E3826-cbg-1"]
 S -->|port 13| P1["PMS-1"]
 S -->|port 14| P0["PMS-0"]
 S -->|port 15| ORCHID["Orchid Controller"]
 S -->|port 16| S1["Switch on Third shelf"]
{{< /mermaid >}}
